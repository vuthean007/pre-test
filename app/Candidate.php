<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $table = 'candidates';
    protected $primaryKey = 'id';


    public function survey()
    {
        return $this->hasOne('App\Survey', 'foreign_key', 'user_id');
    }
}
