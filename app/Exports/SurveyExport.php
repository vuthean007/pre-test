<?php

namespace App\Exports;

use App\Candidate;
use App\Survey;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SurveyExport implements FromCollection,WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return collect(Survey::getAllSurvey());
    }

    public function headings(): array
    {
        return [
            'id',
            'name',
            'gender',
            'phone',
            'apply',
            'total_red',
            'total_blue',
            'total_green',
            'total_yellow',
            'all_surveys',
            'created At'
        ];
    }
}
