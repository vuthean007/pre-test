<?php

namespace App\Http\Controllers;

use App\Color;
use App\Question;
use Illuminate\Http\Request;
// use Symfony\Component\HttpFoundation\Session\Session;
use Illuminate\Support\Facades\Session;

class QuestionController extends Controller
{
    public function readData()
    {
        $option = Question::all();
        return view('Question.view_question', ["option" => $option]);
    }

    public function insert(Request $request)
    {

        $validatedData = $request->validate([
            'Option_1' => 'required',
            'Option_2' => 'required',
            'Option_3' => 'required',
            'Option_4' => 'required',
            'Option_1_khmer' => 'required',
            'Option_2_khmer' => 'required',
            'Option_3_khmer' => 'required',
            'Option_4_khmer' => 'required',
            'color_opt_1' => 'required',
            'color_opt_2' => 'required',
            'color_opt_3' => 'required',
            'color_opt_4' => 'required',
        ]);
        $color = Color::all();
        $option = new Question();
        if (isset($_POST['submit'])) {
            $option_1 = $request->input('Option_1');
            $option_2 = $request->input('Option_2');
            $option_3 = $request->input('Option_3');
            $option_4 = $request->input('Option_4');
            $option_1_khmer = $request->input('Option_1_khmer');
            $option_2_khmer = $request->input('Option_2_khmer');
            $option_3_khmer = $request->input('Option_3_khmer');
            $option_4_khmer = $request->input('Option_4_khmer');
            $color_1 = $request->input('color_opt_1');
            $color_2 = $request->input('color_opt_2');
            $color_3 = $request->input('color_opt_3');
            $color_4 = $request->input('color_opt_4');
            $option->option_1 = $option_1;
            $option->option_2 = $option_2;
            $option->option_3 = $option_3;
            $option->option_4 = $option_4;
            $option->option_1_khmer = $option_1_khmer;
            $option->option_2_khmer = $option_2_khmer;
            $option->option_3_khmer = $option_3_khmer;
            $option->option_4_khmer = $option_4_khmer;
            $option->option_1_color = $color_1;
            $option->option_2_color = $color_2;
            $option->option_3_color = $color_3;
            $option->option_4_color = $color_4;

//            $green_1 = Color::where('yellow',$option_1)->count();
//            $green_2 = Color::where('red',$option_1)->count();
//            $green_3 = Color::where('blue',$option_1)->count();
//            $green_4 = Color::where('green',$option_1)->count();
//            if ($green_1 > 0){
//                $option->option_1_color='';
//                $option->save();
//            }elseif ($green_2 > 0){
//                $option->option_1_color='red';
//                $option->save();
//            }elseif ($green_3 > 0){
//                $option->option_1_color='blue';
//                $option->save();
//            }
//            elseif ($green_4 > 0){
//                $option->option_1_color='green';
//                $option->save();
//            }
//            else{
//                return 'error';
//            }
            $option->save();
        }
        Session::flash('message', 'Your Data has inserted!');
        return redirect('question/view');
    }

    public function edit($id)
    {
        $option = Question::find($id);
        return view('Question.edit_question')->with('option', $option);
    }

    public function update(Request $request, $id)
    {

        $validatedData = $request->validate([
            'Option_1' => 'required',
            'Option_2' => 'required',
            'Option_3' => 'required',
            'Option_4' => 'required',
            'Option_1_khmer' => 'required',
            'Option_2_khmer' => 'required',
            'Option_3_khmer' => 'required',
            'Option_4_khmer' => 'required',
        ]);
        $option = Question::find($id);
        $option->option_1 = $request->input('Option_1');
        $option->option_2 = $request->input('Option_2');
        $option->option_3 = $request->input('Option_3');
        $option->option_4 = $request->input('Option_4');
        $option->option_1_khmer = $request->input('Option_1_khmer');
        $option->option_2_khmer = $request->input('Option_2_khmer');
        $option->option_3_khmer = $request->input('Option_3_khmer');
        $option->option_4_khmer = $request->input('Option_4_khmer');
        $color_1 = $request->input('color_opt_1');
        $color_2 = $request->input('color_opt_2');
        $color_3 = $request->input('color_opt_3');
        $color_4 = $request->input('color_opt_4');
        $option->option_1_color = $color_1;
        $option->option_2_color = $color_2;
        $option->option_3_color = $color_3;
        $option->option_4_color = $color_4;

        $option->save();
        Session::flash('message', 'Your Data has Updated!');
        return redirect('question/view');
    }

    public function destroy($id)
    {
        $option = Question::find($id);
        $option->delete();
        Session::flash('message', 'Your Data has deleted!');
        return back();

    }
}
