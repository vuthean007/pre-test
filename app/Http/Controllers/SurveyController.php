<?php

namespace App\Http\Controllers;

use App\Exports\SurveyExport;
use App\Question;
use App\Survey;
use App\Candidate;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class SurveyController extends Controller
{
    public function submit_survey()
    {
        return view('survey');
    }

    public function review()
    {

        return view('review');
    }

    public function view($id)
    {
        $user = Candidate::find($id);
        $select_survey = Survey::find($id);
        return view('view_survey', compact('select_survey', 'user'));
    }

    public function export()
    {
        return Excel::download(new SurveyExport, 'candidate.xlsx');
    }
    public function search(Request $request){
        $fromDate = $request->input('fromDate');
        $toDate= $request->input('toDate');
        if ($fromDate != null && $toDate != null) {
            $survey = DB::table('candidates')->select('candidates.id', 'candidates.name', 'candidates.gender', 'candidates.phone','candidates.apply',
                'candidates.created_at', 'surveys.total_red', 'surveys.total_blue', 'surveys.total_green', 'surveys.total_yellow', 'surveys.all_surveys')
                ->where(DB::raw("(DATE_FORMAT(candidates.created_at,'%Y-%m-%d'))"), '>=', $fromDate)
                ->where(DB::raw("(DATE_FORMAT(candidates.created_at,'%Y-%m-%d'))"), '<=', $toDate)
                ->join('surveys', 'candidates.id', '=', 'surveys.user_id')
                ->get();
            return view('export_excel', compact('survey'));
        }else{
            $survey = DB::table('candidates')->select('candidates.id', 'candidates.name', 'candidates.gender', 'candidates.phone','candidates.apply',
                'candidates.created_at', 'surveys.total_red', 'surveys.total_blue', 'surveys.total_green', 'surveys.total_yellow', 'surveys.all_surveys')
                ->join('surveys', 'candidates.id', '=', 'surveys.user_id')
                ->get();
            return view('export_excel', compact('survey'));
        }
    }
    public function searchAll(){
        $survey = DB::table('candidates')->select('candidates.id', 'candidates.name', 'candidates.gender', 'candidates.phone','candidates.apply',
            'candidates.created_at', 'surveys.total_red', 'surveys.total_blue', 'surveys.total_green', 'surveys.total_yellow', 'surveys.all_surveys')
            ->join('surveys', 'candidates.id', '=', 'surveys.user_id')
            ->get();
        return view('export_excel', compact('survey'));
    }

    public function insert(Request $request)
    {
        $count_question = Question::count();

        $validate_array = [
            'name' => 'required',
            'sex' => 'required',
            'phone' => 'required|min:9|max:11|regex:[0]',
            'position' => 'required',
            'captcha' => 'required|captcha',
        ];
        for ($i=0; $i<$count_question ;$i++){
            $validate_array['survey.' . $i] = 'required';
        }


        $this->validate($request, $validate_array);

        $select_date = DB::table('candidates')->select('created_at')->where('name',$request->input('name'))
            ->where('phone',$request->input('phone'))->orderBy('created_at', 'desc')->first();
        $mytime = Carbon::now();
        $dd = $mytime->toDateTimeString();
        if ($select_date != null){
            $create_date = $select_date->created_at;
            $start_date = Carbon::parse($create_date);
            $count_date = $start_date->diffInDays($dd);
            if ($count_date >= 31){
                $survey = new Survey();
                $user = new Candidate();
                if (isset($_POST['submit'])) {
                    $user->name = $request->input('name');
                    $user->gender = $request->input('sex');
                    $user->phone = $request->input('phone');
                    $user->apply = $request->input('position');
                    $user->save();
                    $id = $user->id;
                    $survey->user_id = $id;

                    $array = array();
                    foreach ($request->survey as $fn) {
                        array_push($array, $fn);
                    }
                    $survey->all_surveys = serialize($array);
                    $red = $request->input('red');
                    $yellow = $request->input('yellow');
                    $blue = $request->input('blue');
                    $green = $request->input('green');
                    $survey->total_red = $red;
                    $survey->total_blue = $blue;
                    $survey->total_green = $green;
                    $survey->total_yellow = $yellow;
                    if ($red != null || $blue != null || $green != null || $yellow!= null){
                        $success = $survey->save();
                    }else{
                        $option = Candidate::find($id);
                        $option->delete();
                        return redirect('/insert_failed');
                    }

                    $survey_id = $survey->id;
                    $select_survey = Survey::find($survey_id);
                    $user = Survey::find($survey_id)->user;
                    $question = Question::all();
                    if ($success) {
                        Session::put('select_survey', $select_survey);
                        Session::put('user', $user);
                        Session::put('question', $question);
                        return redirect('survey/review');
                    }
                }
            }else{
                return redirect('survey_failed');
            }
        }else{
            $survey = new Survey();
            $user = new Candidate();
            if (isset($_POST['submit'])) {
                $user->name = $request->input('name');
                $user->gender = $request->input('sex');
                $user->phone = $request->input('phone');
                $user->apply = $request->input('position');
                $user->save();
                $id = $user->id;
                $survey->user_id = $id;

                $array = array();
                foreach ($request->survey as $fn) {
                    array_push($array, $fn);
                }
                $survey->all_surveys = serialize($array);
                $red = $request->input('red');
                $yellow = $request->input('yellow');
                $blue = $request->input('blue');
                $green = $request->input('green');
                $survey->total_red = $red;
                $survey->total_blue = $blue;
                $survey->total_green = $green;
                $survey->total_yellow = $yellow;
                if ($red != null || $blue != null || $green != null || $yellow!= null){
                    $success = $survey->save();
                }else{
                    $option = Candidate::find($id);
                    $option->delete();
                    return redirect('/insert_failed');
                }


                $survey_id = $survey->id;
                $select_survey = Survey::find($survey_id);
                $user = Survey::find($survey_id)->user;
                $question = Question::all();
                if ($success) {
                    Session::put('select_survey', $select_survey);
                    Session::put('user', $user);
                    Session::put('question', $question);
                    return redirect('survey/review');
                }
            }
        }
    }
    public function reloadCaptcha()
    {
        return response()->json(['captcha'=> captcha_img('math')]);
    }
}


