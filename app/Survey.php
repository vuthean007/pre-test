<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Survey extends Model
{
    protected $table = 'surveys';
    protected $primaryKey = 'id';


    public function user()
    {
        return $this->belongsTo('App\Candidate');
    }

    public static function getAllSurvey(){
     $records = Candidate::select("candidates.id","candidates.name","candidates.gender","candidates.phone","candidates.apply","surveys.total_red","surveys.total_blue","surveys.total_green",
                            "surveys.total_yellow",("surveys.all_surveys"),DB::raw("(DATE_FORMAT(candidates.created_at,'%Y-%m-%d'))"))
                           ->join("surveys","candidates.id","=", "surveys.user_id")->get();
     return ($records);
    }

}
