<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('option_1');
            $table->string('option_1_khmer');
            $table->string('option_1_color');
            $table->string('option_2');
            $table->string('option_2_khmer');
            $table->string('option_2_color');
            $table->string('option_3');
            $table->string('option_3_khmer');
            $table->string('option_3_color');
            $table->string('option_4');
            $table->string('option_4_khmer');
            $table->string('option_4_color');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
