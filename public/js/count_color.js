

$(":radio").on("change", function(){
    var red = Number($('red').text());
    var blue = Number($('blue').text());
    var green = Number($('green').text());
    var yellow = Number($('yellow').text());
    $(":radio:checked").each(function(){
        if($(this).data('color') == 'red'){
            red += 1;
        }
    });
    $(":radio:checked").each(function(){
        if($(this).data('color') == 'blue'){
            blue += 1;
        }
    });
    $(":radio:checked").each(function(){
        if($(this).data('color') == 'green'){
            green += 1;
        }
    });
    $(":radio:checked").each(function(){
        if($(this).data('color') == 'yellow'){
            yellow += 1;
        }
    });

    $("#green").text(green);
    $("#yellow").text(yellow);
    $("#red").text(red);
    $("#blue").text(blue);
    $(":radio:checked").each(function(){
        var data = $('#red').html();
        $('#input_red').val(data);
        var data = $('#blue').html();
        $('#input_blue').val(data);
        var data = $('#green').html();
        $('#input_green').val(data);
        var data = $('#yellow').html();
        $('#input_yellow').val(data);
    });
});
