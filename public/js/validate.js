$(document).ready(function () {

    $('#phone').blur(function (e) {
        var intRegex = /^\d+$/;
        var floatRegex = /^((\d+(\.\d *)?)|((\d*\.)?\d+))$/;
        var str = $(this).val();
        if (intRegex.test(str) || floatRegex.test(str)) {
            $(this).css('border', '1px solid Gainsboro')
        } else {

            alert('This Field required Number    ');

            $(this).css('border', '1px solid red');
        }
        if (str.length < 9 || str.length > 11) {
            $(this).css('border', '1px solid red');
            alert('phone number minimum 9 and maximum 11');
        } else {
            $(this).css('border', '1px solid Gainsboro');
        }
        if (str.charAt(0) != 0) {
            alert('This field should start with 0 digit')
        }
    });
    $('#name').change(function (){
        if ($('#name').val() !== ''){
            $('#required_name').html('');
        }
    });
    $('#phone').change(function (){
        if ($('#phone').val() !== ''){
            $('#required_phone').html('');
        }
    });
    $('#position').change(function (){
        if ($('#position').val() !== ''){
            $('#required_position').html('');
        }
    });
    $('input[name=sex]').click(function (){
        if ($('input[name=sex]').val() !== ''){
            $('#required_sex').html('');
        }
    });

// count Color
    $(":radio").on("change", function () {
        var red = Number($('red').text());
        var blue = Number($('blue').text());
        var green = Number($('green').text());
        var yellow = Number($('yellow').text());
        $(":radio:checked").each(function () {
            if ($(this).data('color') == 'red') {
                red += 1;
            }
        });
        $(":radio:checked").each(function () {
            if ($(this).data('color') == 'blue') {
                blue += 1;
            }
        });
        $(":radio:checked").each(function () {
            if ($(this).data('color') == 'green') {
                green += 1;
            }
        });
        $(":radio:checked").each(function () {
            if ($(this).data('color') == 'yellow') {
                yellow += 1;
            }
        });

        $("#green").text(green);
        $("#yellow").text(yellow);
        $("#red").text(red);
        $("#blue").text(blue);
        $(":radio:checked").each(function () {
            var data = $('#red').html();
            $('#input_red').val(data);
            var data = $('#blue').html();
            $('#input_blue').val(data);
            var data = $('#green').html();
            $('#input_green').val(data);
            var data = $('#yellow').html();
            $('#input_yellow').val(data);
        });
    });
// validate front-end
    var message = '#message_';
    var option = '#Option_';
    var khmer = '_khmer';
    var color = '#color_';
    var colors = 'color_';

    $('form').submit(function (e) {
        for (var i = 1; i <= 4; i++) {
            if ($(option + i).val() === '') {
                e.preventDefault();
                $(message + i).html('*');
                $(message + i).css('color', 'red');
                $(option + i).css('border', '2px solid red');
                $(option + i).focus();

            }
        }

        $('#Option_1').keyup(function () {
            $('#message_1').html('');
            $('#Option_1').css('border', '2px solid green');
        });
        $('#Option_2').keyup(function () {
            $('#message_2').html('');
            $('#Option_2').css('border', '2px solid green');
        });
        $('#Option_3').keyup(function () {
            $('#message_3').html('');
            $('#Option_3').css('border', '2px solid green');
        });
        $('#Option_4').keyup(function () {
            $('#message_4').html('');
            $('#Option_4').css('border', '2px solid green');
        });
        for (var i = 1; i <= 4; i++) {
            if ($(option + i + khmer).val() === '') {
                e.preventDefault();
                $(message + i + khmer).html('*');
                $(message + i + khmer).css('color', 'red');
                $(option + i + khmer).css('border', '2px solid red');
                $(option + i + khmer).focus();
            }
        }
        $('#Option_1_khmer').keyup(function () {
            $('#message_1_khmer').html('');
            $('#Option_1_khmer').css('border', '2px solid green');
        });
        $('#Option_2_khmer').keyup(function () {
            $('#message_2_khmer').html('');
            $('#Option_2_khmer').css('border', '2px solid green');
        });
        $('#Option_3_khmer').keyup(function () {
            $('#message_3_khmer').html('');
            $('#Option_3_khmer').css('border', '2px solid green');
        });
        $('#Option_4_khmer').keyup(function () {
            $('#message_4_khmer').html('');
            $('#Option_4_khmer').css('border', '2px solid green');
        });
        for (var j = 1; j <= 4; j++) {
            if ($(color + j).val() === '') {
                e.preventDefault();
                $(message + colors + j).html('*');
                $(message + colors + j).css('color', 'red');
                $(color + j).css('border', '2px solid red');
                $(color + j).focus();
            }
        }
        $('#color_1').click(function () {
            $('#message_color_1').html('');
            $('#color_1').css('border', '2px solid green');
        });
        $('#color_2').click(function () {
            $('#message_color_2').html('');
            $('#color_2').css('border', '2px solid green');
        });
        $('#color_3').click(function () {
            $('#message_color_3').html('');
            $('#color_3').css('border', '2px solid green');
        });
        $('#color_4').click(function () {
            $('#message_color_4').html('');
            $('#color_4').css('border', '2px solid green');
        });

        //
        // for (var j = 1; i <= 4; i++) {
        //     if ($(color+i).val() === '') {
        //         e.preventDefault();
        //         $(message+colors+i).html('*');
        //         $(message+colors+i).css('color','red');
        //         $(color+i).css('border', '2px solid red');
        //         $(color+i).focus();
        //     }
        // }

        if ($('#Option_1').val() === '' || $('#Option_2').val() === '' || $('#Option_3').val() === '' || $('#Option_4').val() === '' ||
            $('#Option_1_khmer').val() === '' || $('#Option_2_khmer').val() === '' || $('#Option_3_khmer').val() === '' ||
            $('#Option_4_khmer').val() === '') {
            alert('Please Fill in Field Required!');
        }
    });

    // validate khmer only

    function checkKhmerCharacters(val) {
        // Khmer range found at https://en.wikipedia.org/wiki/Khmer_alphabet
        return /[ក-\u17fe᧠-᧾]/.test(val);
    }

    // Validate as you type
    $('#Option_1_khmer').on('input', function (e) {
        const elm = $(this);
        const isValid = checkKhmerCharacters(e.target.value);
        elm.toggleClass('input-error', !isValid);
    });

    $('#Option_1_khmer').on('blur', () => {
        var namekh = $('#Option_1_khmer').val();
        console.log(checkKhmerCharacters(namekh));

        if (!checkKhmerCharacters(namekh) && $('#Option_1_khmer').val() != '') {
            alert('please enter khmer unicode only');
            $('#Option_1_khmer').css('border', '2px solid red')
            $('#btn').attr('type', 'button');
        }
        if (checkKhmerCharacters(namekh) && $('#Option_1_khmer').val() != '') {
            $('#Option_1_khmer').css('border', '2px solid green')
            $('#btn').attr('type', 'submit');
            $('#btn').preventDefault();

        }
    });

    $('#Option_2_khmer').on('input', function (e) {
        const elm = $(this);
        const isValid = checkKhmerCharacters(e.target.value);
        elm.toggleClass('input-error', !isValid);
    });

    $('#Option_2_khmer').on('blur', () => {
        var namekh = $('#Option_2_khmer').val();
        console.log(checkKhmerCharacters(namekh));

        if (!checkKhmerCharacters(namekh) && $('#Option_2_khmer').val() != '') {
            alert('please enter khmer unicode only');
            $('#Option_2_khmer').css('border', '2px solid red')
            $('#btn').data('block', 'submit');
            $('#btn').attr('type', 'button');
        }
        if (checkKhmerCharacters(namekh) && $('#Option_2_khmer').val() != '') {
            $('#Option_2_khmer').css('border', '2px solid green')
            $('#btn').data('block', 'button');
            $('#btn').attr('type', 'submit');

        }
    });

    $('#Option_3_khmer').on('input', function (e) {
        const elm = $(this);
        const isValid = checkKhmerCharacters(e.target.value);
        elm.toggleClass('input-error', !isValid);
    });

    $('#Option_3_khmer').on('blur', () => {
        var namekh = $('#Option_3_khmer').val();
        console.log(checkKhmerCharacters(namekh));

        if (!checkKhmerCharacters(namekh) && $('#Option_3_khmer').val() != '') {
            alert('please enter khmer unicode only');
            $('#Option_3_khmer').css('border', '2px solid red')
            $('#btn').data('block', 'submit');
            $('#btn').attr('type', 'button');
        }
        if (checkKhmerCharacters(namekh) && $('#Option_3_khmer').val() != '') {
            $('#Option_3_khmer').css('border', '2px solid green')
            $('#btn').data('block', 'button');
            $('#btn').attr('type', 'submit');

        }
    });

    $('#Option_4_khmer').on('input', function (e) {
        const elm = $(this);
        const isValid = checkKhmerCharacters(e.target.value);
        elm.toggleClass('input-error', !isValid);
    });

    $('#Option_4_khmer').on('blur', () => {
        var namekh = $('#Option_4_khmer').val();
        console.log(checkKhmerCharacters(namekh));

        if (!checkKhmerCharacters(namekh) && $('#Option_4_khmer').val() != '') {
            alert('please enter khmer unicode only');
            $('#Option_4_khmer').css('border', '2px solid red')
            $('#btn').data('block', 'submit');
            $('#btn').attr('type', 'button');
        }
        if (checkKhmerCharacters(namekh) && $('#Option_4_khmer').val() != '') {
            $('#Option_4_khmer').css('border', '2px solid green')
            $('#btn').data('block', 'button');
            $('#btn').attr('type', 'submit');

        }
    });


    // <<<<<>>>>>>>
    // validate backend
    // if ($('mess_input').val() !== ''){
    //     $('#Option_1').attr('required','true');
    //     $('#Option_2').attr('required','true');
    //     $('#Option_3').attr('required','true');
    //     $('#Option_4').attr('required','true');
    //     $('#Option_1_khmer').attr('required','true');
    //     $('#Option_2_khmer').attr('required','true');
    //     $('#Option_3_khmer').attr('required','true');
    //     $('#Option_4_khmer').attr('required','true');
    // }
    // <<<<<<>>>>>>>
    $('#myTable').DataTable();
})
