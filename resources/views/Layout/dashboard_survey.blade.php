<head>
    <title>Survey</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link href='https://fonts.googleapis.com/css?family=Bayon' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{asset('css/survey.css')}}">
    <link rel="stylesheet" href="{{asset('css/alert.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables.min.css')}}">
    <style>
        .center {
            margin-top: 15%;
            margin-left: 25%;
            margin-right: 25%;
        }

        div.sticky {
            position: -webkit-sticky;
            position: sticky;
            top: 0;
            padding-top: 0.5%;
            opacity: 0.8;
            z-index: 1;
        }
    </style>
</head>
<body>
{{--<nav class="navbar navbar-inverse navbar-fixed-top">--}}
{{--    <div class="container-fluid">--}}
{{--        <div class="navbar-header">--}}
{{--            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">--}}
{{--                <span class="icon-bar"></span>--}}
{{--                <span class="icon-bar"></span>--}}
{{--                <span class="icon-bar"></span>--}}
{{--            </button>--}}
{{--        </div>--}}
{{--        <div class="collapse navbar-collapse" id="myNavbar">--}}
{{--            <ul class="nav navbar-nav">--}}
{{--                <li class=""><a href="{{url('/')}}">Home</a></li>--}}
{{--                <li><a href="{{url('/survey')}}">Survey</a></li>--}}
{{--                <li><a href="{{url('/question/view')}}">Question</a></li>--}}
{{--            </ul>--}}
{{--            --}}{{--            <ul class="nav navbar-nav navbar-right">--}}
{{--            --}}{{--                <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>--}}
{{--            --}}{{--                <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>--}}
{{--            --}}{{--            </ul>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</nav>--}}
@yield('content')
<script src="{{asset('js/alert.js')}}"></script>
<script src="{{asset('js/validate.js')}}"></script>
<script src="{{asset('js/datatables.min.js')}}"></script>
<script src="{{asset('js/count_color.js')}}"></script>
</body>
</html>

