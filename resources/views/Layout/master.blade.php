<head>
    <title>Survey</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link href='https://fonts.googleapis.com/css?family=Bayon' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{asset('css/survey.css')}}">
    <link rel="stylesheet" href="{{asset('css/alert.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables.min.css')}}">
    <style>
        @media print {
            #back, #download {
                visibility: hidden;
            }
        }
        .center {
            margin-top: 15%;
            margin-left: 25%;
            margin-right: 25%;
        }

        div.sticky {
            position: -webkit-sticky;
            position: sticky;
            top: 0;
            padding-top: 4%;
            opacity: 0.8;
            z-index: 1;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class=""><a href="{{url('/dashboard')}}">Home</a></li>
                <li><a href="{{url('/question/view')}}">Question</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
               <li>
                   <a id="navbarDropdown" class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre src="{{asset('image/user.png')}}" width="10%">
{{--                                       {{ Auth::user()->name }}--}}
                       <i style="font-size: 200%" class="fa fa-user-circle-o" aria-hidden="true"></i>
                   </a>

                   <div class="dropdown-menu dropdown-menu-right " aria-labelledby="navbarDropdown">
                       <a class="dropdown-item btn text-white" href="{{ route('logout') }}"
                          onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                           {{ __('Logout') }}
                       </a>
                       <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                           @csrf
                       </form>
                       <a href="{{url('change-password')}}" class="btn text-white">Change Password</a>
                   </div>
               </li>
{{--                <li>--}}
{{--                    <a href="{{ route('logout') }}"  onclick="event.preventDefault();--}}
{{--                                                     document.getElementById('logout-form').submit();"><span class="glyphicon glyphicon-user"></span> Log Up</a>--}}
{{--                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">--}}
{{--                        @csrf--}}
{{--                    </form>--}}
{{--                </li>--}}
            </ul>
        </div>
    </div>
</nav>
@yield('content')
<script src="{{asset('js/alert.js')}}"></script>
<script src="{{asset('js/validate.js')}}"></script>
<script src="{{asset('js/datatables.min.js')}}"></script>
<script src="{{asset('js/count_color.js')}}"></script>
<script src="{{asset('js/pdf.js')}}"></script>




</body>
</html>
