@extends('Layout.master')
@section('content')

    @if(Session::has('message'))
        <div class="alert w3-green col-sm-3 pull-right myAlert-bottom" role="alert" style="z-index: 2;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
            <strong>Success!</strong> {{ Session::get('message') }}
        </div>
    @endif
    <div class="sticky">
        <a href="{{url('/question')}}" class="btn w3-indigo w3-margin-bottom w3-margin-right w3-round w3-right"><i
                class="fa fa-plus-circle" aria-hidden="true"> </i> New</a>
    </div>
    <div class="w3-container" style="margin-top:70px">
        <div class="w3-card-4 w3-round">
            <header class="w3-container text-center  w3-round">
                <h1 class="h1">List of Questions </h1>
            </header>
        </div>
        <br>
        <div class="w3-container w3-margin-bottom">
            <table id="myTable" class="table table-hover">
                <thead>
                <tr>
                    <th>NO.</th>
                    <th>Option 1</th>
                    <th>Option 2</th>
                    <th>Option 3</th>
                    <th>Option 4</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1; ?>
                @foreach(\App\Question::all() as $data)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$data->option_1}}</td>
                        <td>{{$data->option_2}}</td>
                        <td>{{$data->option_3}}</td>
                        <td>{{$data->option_4}}</td>
                        <td class="row">
                            <a class="btn w3-indigo col-sm-2" href="{{url('question/edit',$data->id)}}">Edit</a>
                            <form class="col-sm-2" method="POST" action="{{ url('question/delete', $data->id) }}">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button onclick="return  confirm('Do you want Delete this?')" type="submit"
                                        class="btn w3-indigo">Delete
                                </button>
                            </form>
                            {{--                                <a href="{{url('question/delete',$data->id)}}">Delete</a>--}}
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

    <script>

    </script>
@stop

