@extends('Layout.dashboard_survey')
@section('content')
    <center>
        <div class="w3-card-4 rounded col-sm-5 center w3-round">
            <div class="rounded-top w3-container  font-weight-bold ">
                <h2>
                    <i class="fa fa-check-circle" aria-hidden="true"></i>
                    <b class="text-danger">Thank You for Your Information</b>
                </h2>
            </div>
            <hr>

                <a  href="{{url('/dashboard')}}" class="btn w3-indigo text-white"><i class="fa fa-undo"></i> More</a><br><br>
        </div>
    </center>
@stop
