@extends('layouts.app')

@section('content')
    <div class="container h-100">
        <div class="d-flex justify-content-center h-100">
            <div class="user_card">
                <div class="d-flex justify-content-center">
                    <div class="brand_logo_container">
                        <img src="https://is4-ssl.mzstatic.com/image/thumb/Purple113/v4/52/09/cf/5209cf11-1055-93d5-48f2-d42de82aa679/source/512x512bb.jpg" class="brand_logo" alt="Logo">
                    </div>
                </div>
                <div class="d-flex justify-content-center form_container">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="input-group mb-3">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input type="email" name="email" id="email" class="form-control input_user @error('email') is-invalid @enderror" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="input-group mb-2">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="fas fa-key"></i></span>
                            </div>
                            <input type="password" name="password" id="password" class="form-control input_user @error('password') is-invalid @enderror" value="{{ old('password') }}" required autocomplete="password" autofocus placeholder="Password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
{{--                        <div class="form-group">--}}
{{--                            <div class="custom-control custom-checkbox">--}}
{{--                                <input type="checkbox" class="custom-control-input" id="customControlInline">--}}
{{--                                <label class="custom-control-label text-white" for="customControlInline">Remember me</label>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="d-flex justify-content-center mt-3 login_container">
                            <button type="submit" name="button" class="btn login_btn">Login</button>
                        </div>
                    </form>
                </div>

                <div class="mt-4">
{{--                    <div class="d-flex justify-content-center links text-white">--}}
{{--                        Don't have an account? <a href="{{ route('register') }}" class="ml-2">Sign Up</a>--}}
{{--                    </div>--}}
{{--                    <div class="d-flex justify-content-center links">--}}
{{--                        @if (Route::has('password.request'))--}}
{{--                            <a href="{{ route('password.request') }}">Forgot your password?</a>--}}
{{--                        @endif--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>
@endsection
