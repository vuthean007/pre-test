@if(Auth::user()->first_login === 'yes')
    <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/login.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
          integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
<div id="app">
    <main class="py-4">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Change {{Auth::user()->name}} ' s Password with Current Password</div>

                        <div class="card-body">
                            <form method="POST" action="{{ route('change.password') }}">
                                @csrf

                                @foreach ($errors->all() as $error)
                                    <p class="text-danger">{{ $error }}</p>
                                @endforeach

                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">Current
                                        Password</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control"
                                               name="current_password" autocomplete="current-password">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">New
                                        Password</label>

                                    <div class="col-md-6">
                                        <input id="new_password" type="password" class="form-control"
                                               name="new_password" autocomplete="current-password">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">New Confirm
                                        Password</label>

                                    <div class="col-md-6">
                                        <input id="new_confirm_password" type="password" class="form-control"
                                               name="new_confirm_password" autocomplete="current-password">
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn w3-orange">
                                            Update Password
                                        </button> or
                                        <a href="{{ route('logout') }}"  onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();" class="btn w3-indigo"><span class="glyphicon glyphicon-user"></span> Log Up</a>
                                    </div>
                                </div>
                            </form>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
</body>
</html>
@else
    <head>
        <title>Survey</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link href='https://fonts.googleapis.com/css?family=Bayon' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="{{asset('css/survey.css')}}">
        <link rel="stylesheet" href="{{asset('css/alert.css')}}">
        <link rel="stylesheet" href="{{asset('css/Datatable/datatable.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/Datatable/style.css')}}">
        <style>
            .center {
                margin-top: 15%;
                margin-left: 25%;
                margin-right: 25%;
            }

            div.sticky {
                position: -webkit-sticky;
                position: sticky;
                top: 0;
                padding-top: 4%;
                opacity: 0.8;
                z-index: 1;
            }
        </style>
    </head>
    <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li class=""><a href="{{url('/dashboard')}}">Home</a></li>
                    <li><a href="{{url('/question/view')}}">Question</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" role="button" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false" v-pre src="{{asset('image/user.png')}}"
                           width="10%">
                            {{--                                       {{ Auth::user()->name }}--}}
                            <i style="font-size: 200%" class="fa fa-user-circle-o" aria-hidden="true"></i>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right " aria-labelledby="navbarDropdown">
                            <a class="dropdown-item btn text-white" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                            <a href="{{url('change-password')}}" class="btn text-white">Change Password</a>
                        </div>
                    </li>
                    {{--                <li>--}}
                    {{--                    <a href="{{ route('logout') }}"  onclick="event.preventDefault();--}}
                    {{--                                                     document.getElementById('logout-form').submit();"><span class="glyphicon glyphicon-user"></span> Log Up</a>--}}
                    {{--                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">--}}
                    {{--                        @csrf--}}
                    {{--                    </form>--}}
                    {{--                </li>--}}
                </ul>
            </div>
        </div>
    </nav>
    @if(Session::has('select_surveys'))
        <div class="alert w3-green col-sm-3 pull-right myAlert-bottom" role="alert" style="z-index: 6;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
            <strong>Success!</strong> {{ Session::get('select_surveys') }}
        </div>
    @endif
    <div class="w3-container" style="margin-top:70px">
        <div class="w3-card-4 w3-round">
            <header class="w3-container text-center radius_header">
                <h1 class="h1">All Surveys View </h1>
            </header>
            <form action="{{url('search')}}" method="post">
                @csrf
                <div class="container">
                    <div class="row">
                        <div class="container-fluid">
                            <div class="form-group row">
                                <div class="col-sm-7">
                                    <h1 class="">Please Select Date for Export </h1>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-1">
                                    <b>From</b>
                                </div>
                                {{--                                <label for="date" class="col-form-label- col-ms-2">Created From</label>--}}
                                <div class="col-sm-3">
                                    <input type="date" class="form-control input-sm" id="fromDate" name="fromDate">
                                </div>
                                <div class="col-sm-1">
                                    <b>To</b>
                                </div>
                                {{--                                <label for="date" class="col-form-label- col-ms-2">Created To</label>--}}
                                <div class="col-sm-3">
                                    <input type="date" class="form-control input-sm" id="toDate" name="toDate">
                                </div>
                                <div class="col-sm-3">
                                    <button type="submit" class="btn w3-indigo" name="search" title="Search"><i class="fa fa-search" aria-hidden="true"></i></button>
                                    <a  href="{{url('search_all')}}">Select All for export</a>
                                    {{--                                        <input type="submit" class="btn" name="search" title="Search" value="Search">--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="w3-container w3-margin-bottom" style="padding: 3%;">

                <table id="example" class="table table-bordered">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Candidate Name</th>
                        <th>Apply for</th>
                        <th>Created</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                    @foreach($survey as $data)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$data->name}}</td>
                            <td>{{$data->apply}}</td>
                            <td>{{$data->created_at}}</td>
                            <td>
                                <a href="{{url('/survey/view',$data->id)}}" class="btn w3-indigo"><i class="fa fa-eye"
                                                                                                     aria-hidden="true"></i>View</a>
{{--                                <a href="{{url('survey/export_pdf',$data->id)}}" class="btn w3-indigo"><i class="fa fa-print"></i> PDF</a>--}}
                            </td>

                        </tr>
                    @endforeach

                    </tbody>
                </table>



            </div>
        </div>

    </div>

    <script src="{{asset('js/alert.js')}}"></script>
    <script src="{{asset('js/validate.js')}}"></script>
    <script src="{{asset('js/Datatable/datatable.min.js')}}"></script>
    <script src="{{asset('js/count_color.js')}}"></script>
    <script>
        $(document).ready(function(){
            $('#example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    // { extend: 'copy'},
                    // {extend: 'csv'},
                    {extend: 'excel', title: 'List Candidates'},
                    // {extend: 'pdf', title: 'ExampleFile'},
                    //
                    // {extend: 'print',
                    //     customize: function (win){
                    //         $(win.document.body).addClass('white-bg');
                    //         $(win.document.body).css('font-size', '10px');
                    //
                    //         $(win.document.body).find('table')
                    //             .addClass('compact')
                    //             .css('font-size', 'inherit');
                    //     }
                    // }
                ]
            });
        });

    </script>
    </body>
@endif
