@extends('Layout.dashboard_survey')
@section('content')


        <center>
            <div class="w3-card-4 rounded col-sm-5 center w3-round">
                <div class="rounded-top w3-container  font-weight-bold ">
                    <h2>
                        <i class="fa fa-exclamation-circle fa-3x" aria-hidden="true"></i>
                        <p class="">You can't submit now. Please go back.</p>
                    </h2>
                </div>
                <hr>

                <a  href="{{url('/')}}" class="btn w3-indigo text-white"><i class="fa fa-undo"></i> Back</a><br><br>
            </div>
        </center>
  
      
@stop
