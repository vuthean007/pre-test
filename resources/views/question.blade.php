@extends('Layout.master')
@section('content')
    <a href="{{url('/question/view')}}" class="btn w3-indigo w3-margin-bottom w3-margin-right w3-round w3-right"><i
            class="fa fa-list" aria-hidden="true"> </i> List</a>
    <div class="w3-container" style="margin-top:70px">
        <div class="w3-card-4 w3-round">
            <header class="w3-container text-center radius_header">
                <h1 class="h1">Add Question({{$count_question+1}}) </h1>
            </header>
            <form name class="form-horizontal padding_form" method="post" action="{{url('/question/insert')}}">
                <center>
                    @csrf
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="name">Option_1 <i id="message_1"></i>
                            @error('Option_1')
                            <i style="color: red">*</i>
                            @enderror
                            :</label>
                        <div class="col-sm-8 form-inline ">
                            <input type="text" class="form-control @error('option_1') is-invalid @enderror"
                                   id="Option_1" placeholder="Enter Option 1" name="Option_1">
                            <label class="" for="name">Option_1_Khmer <i id="message_1_khmer"></i>
                                @error('Option_1_khmer')
                                <i style="color: red">*</i>
                                @enderror
                                :</label>
                            <input type="text" class="form-control @error('option_1_khmer') is-invalid @enderror"
                                   id="Option_1_khmer" placeholder="Enter Khmer" name="Option_1_khmer">
                            <div class="input-group mb-3">
                                <select name="color_opt_1" class="custom-select form-control" id="color_1"
                                        style="border-radius: 3px;">
                                    <option selected value="">Select color for Option 1</option>
                                    <option value="yellow">Yellow</option>
                                    <option value="red">Red</option>
                                    <option value="blue">Blue</option>
                                    <option value="green">Green</option>
                                </select>
                                @error('color_opt_1')
                                <i style="color: red">*</i>
                                @enderror
                            </div>
                        </div>
                        <i id="message_color_1"></i>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Option_2 <i id="message_2"></i>
                            @error('Option_2')
                            <i style="color: red">*</i>
                            @enderror
                            :</label>
                        <div class="col-sm-8 form-inline ">
                            <input type="text" class="form-control @error('option_2') is-invalid @enderror"
                                   id="Option_2" placeholder="Enter Option 2" name="Option_2">
                            <label class="" for="name">Option_2_Khmer <i id="message_2_khmer"></i>
                                @error('Option_1_khmer')
                                <i style="color: red">*</i>
                                @enderror
                                :</label>
                            <input type="text" class="form-control @error('option_2_khmer') is-invalid @enderror"
                                   id="Option_2_khmer" placeholder="Enter Khmer" name="Option_2_khmer">
                            <div class="input-group mb-3">
                                <select name="color_opt_2" class="custom-select form-control" id="color_2"
                                        style="border-radius: 3px;">
                                    <option selected value="">Select color for Option 2</option>
                                    <option value="yellow">Yellow</option>
                                    <option value="red">Red</option>
                                    <option value="blue">Blue</option>
                                    <option value="green">Green</option>
                                </select>
                                @error('color_opt_2')
                                <i style="color: red">*</i>
                                @enderror
                            </div>
                        </div>
                        <i id="message_color_2"></i>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Option_3 <i id="message_3"></i>
                            @error('Option_3')
                            <i style="color: red">*</i>
                            @enderror
                            :</label>
                        <div class="col-sm-8 form-inline ">
                            <input type="text" class="form-control @error('option_3') is-invalid @enderror"
                                   id="Option_3" placeholder="Enter Option 3" name="Option_3">
                            <label class="" for="name">Option_3_Khmer <i id="message_3_khmer"></i>
                                @error('Option_3_khmer')
                                <i style="color: red">*</i>
                                @enderror
                                :</label>
                            <input type="text" class="form-control @error('option_3_khmer') is-invalid @enderror"
                                   id="Option_3_khmer" placeholder="Enter Khmer" name="Option_3_khmer">
                            <div class="input-group mb-3">
                                <select name="color_opt_3" class="custom-select form-control" id="color_3"
                                        style="border-radius: 3px;">
                                    <option selected value="">Select color for Option 3</option>
                                    <option value="yellow">Yellow</option>
                                    <option value="red">Red</option>
                                    <option value="blue">Blue</option>
                                    <option value="green">Green</option>
                                </select>
                                @error('color_opt_3')
                                <i style="color: red">*</i>
                                @enderror
                            </div>
                        </div>
                        <i id="message_color_3"></i>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Option_4 <i id="message_4"></i>
                            @error('Option_4')
                            <i style="color: red">*</i>
                            @enderror
                            :</label>
                        <div class="col-sm-8 form-inline ">
                            <input type="text" class="form-control @error('option_4') is-invalid @enderror"
                                   id="Option_4" placeholder="Enter Option 4" name="Option_4">
                            <label class="" for="name">Option_4_Khmer <i id="message_4_khmer"></i>
                                @error('Option_4_khmer')
                                <i style="color: red">*</i>
                                @enderror
                                :</label>
                            <input type="text" class="form-control @error('option_4_khmer') is-invalid @enderror"
                                   id="Option_4_khmer" placeholder="Enter Khmer" name="Option_4_khmer">
                            <div class="input-group mb-3">
                                <select name="color_opt_4" class="custom-select form-control " id="color_4"
                                        style="border-radius: 3px;">
                                    <option selected value="">Select color for Option 4</option>
                                    <option value="yellow">Yellow</option>
                                    <option value="red">Red</option>
                                    <option value="blue">Blue</option>
                                    <option value="green">Green</option>
                                </select>
                                @error('color_opt_4')
                                <i style="color: red">*</i>
                                @enderror
                            </div>
                        </div>
                        <i id="message_color_4"></i>
                    </div>
                    <button id="btn" type="submit" name="submit" class="btn w3-indigo">Submit</button>
                </center>

            </form>
        </div>
    </div>
    <script>
    </script>
@stop

