@extends('Layout.dashboard_survey')
@section('content')
    <div class="w3-container" style="margin-top:20px">
        <div class="w3-card-4 w3-round">
            <header class="w3-container text-center radius_header">
                <h1 class="h1">Pre-Employment Test </h1>
            </header>
            <form method="post" class="form-horizontal padding_form" action="{{url('survey/insert')}}">
                @csrf
                <div class="form-group">
                    <label class="control-label col-sm-2" for="name" >Candidate Name <i id="required_name" style="color: red">*</i>:
                        @error('name')
                        <i style="color: red">*</i>
                        @enderror
                    </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name"
                               value="{{ old('name') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="sex">Sex <i id="required_sex" style="color: red">*</i>:
                        @error('sex')
                        <i style="color: red">*</i>
                        @enderror
                    </label>
                    <div class="col-sm-8">
                        <label class="radio-inline"><input type="radio" id="sex" name="sex"
                                                           value="male" {{ (old('sex') == 'male') ? 'checked' : ''}}>Male</label>
                        <label class="radio-inline"><input type="radio" id="sex" name="sex"
                                                           value="female" {{ (old('sex') == 'female') ? 'checked' : ''}}>Female</label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="phone">Phone Number <i id="required_phone" style="color: red">*</i>:
                        @error('phone')
                        <i style="color: red">*</i>
                        @enderror
                    </label>
                    <div class="col-sm-8">
                        <input type="tel" class="form-control" id="phone" placeholder="Enter Phone" name="phone"
                               value="{{ old('phone') }}" data-country="Cambodia">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="position">Position Apply <i id="required_position" style="color: red">*</i>:
                        @error('position')
                        <i style="color: red">*</i>
                        @enderror
                    </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="position" placeholder="Enter Position"
                               name="position" value="{{ old('position') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="position"></label>
                    <div class="col-sm-8">
                        <div class="captcha">
                            <span class="w3-round">{!! captcha_img('math') !!}</span>

                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="position"></label>
                    <div class="col-sm-8 ">
                        <div class="form-inline ">
                            <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha">
                            <button type="button" class="btn w3-indigo reload" id="reload">
                                <i class="fa fa-refresh" aria-hidden="true"></i> Refresh
                            </button>
                        </div>
                        @error('captcha')
                        <span style="color: red">{{$message}}</span>
                        @enderror
                    </div>
                </div>

                <div class="container">
                    <h3><b>Part one- Your behavioral profile</b></h3>
                    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Each row of four words across, place an “X” in front of the one
                        word that most often applies to you.
                        Continue through all forty lines ensuring that each number is marked.
                        គូសសញ្ញា“X”ខាងមុខពាក្យមួយដែលសមស្របជាមួយអ្នកបំផុត ក្នុងចំនោមពាក្យទាំងបួនក្នុងជួរនីមួយៗ។
                        ត្រូវបានគូសគ្រប់ទាំងអស់ ៤០ជួរ។
                        If you are not sure which word” most applies” think of what your answer would have been when you
                        were a child?
                        ប្រសិនបើអ្នកមិនប្រាកដថាពាក្យណាមួយសមស្របជាមួយអ្នកបំផុតទេ
                        គិតពីអ្វីដែលអ្នកអាចជាចំលើយរបស់អ្នកកាលពីនៅក្មេង។
                    </p>
                </div>
                <div class="font_size">
                    <?php
                    $i = 1;
                    $n = 1;
                    $k = 1;
                    $m = 1;
                    $j = 1;
                    $message = 1;
                    $opt1_checked = 1;
                    $opt2_checked = 1;
                    $opt3_checked = 1;
                    $opt4_checked = 1;
                    ?>
                    @foreach($survey as $key => $data)
                        <div class="row bottom_padding">
                            <label class="control-label col-sm-1" for="sex"><?=$i++?>:
                                @error('survey.'.$key)
                                <div class="alert w3-red col-sm-3 pull-right myAlert-bottom" role="alert"
                                     style="z-index: 2;">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                    <i class="fa fa-exclamation-circle" aria-hidden="true"></i> Please Fill all fields required.
                                </div>
                                <i style="color: red">*</i>
                                @enderror


                            </label>
                            <label class="radio-inline col-sm-2"><input type="radio" id="sex"
                                                                        data-color="{{$data->option_1_color}}"
                                                                        name="survey[{{$key}}]"
                                                                        value="{{$data->option_1}}" {{ (old('survey.'.$key) === $data->option_1) ? 'checked' : ''}}>
                                <p>{{$data->option_1}}
                                    <br>{{$data->option_1_khmer}}</p></label>
                            <label class="radio-inline col-sm-3"><input type="radio" id="sex"
                                                                        data-color="{{$data->option_2_color}}"
                                                                        name="survey[{{$key}}]"
                                                                        value="{{$data->option_2}}" {{ (old('survey.'.$key) === $data->option_2) ? 'checked' : ''}}>
                                <p>{{$data->option_2}}
                                    <br>{{$data->option_2_khmer}}</p></label>
                            <label class="radio-inline col-sm-3"><input type="radio" id="sex"
                                                                        data-color="{{$data->option_3_color}}"
                                                                        name="survey[{{$key}}]"
                                                                        value="{{$data->option_3}}" {{ (old('survey.'.$key) === $data->option_3) ? 'checked' : ''}}>
                                <p>{{$data->option_3}}
                                    <br>{{$data->option_3_khmer}}</p></label>
                            <label class="radio-inline col-sm-2"><input type="radio" id="sex"
                                                                        data-color="{{$data->option_4_color}}"
                                                                        name="survey[{{$key}}]"
                                                                        value="{{$data->option_4}}" {{ (old('survey.'.$key) === $data->option_4) ? 'checked' : ''}}>
                                <p>{{$data->option_4}}
                                    <br>{{$data->option_4_khmer}}</p></label>
                        </div>
                    @endforeach

                    {{--                        get count of all colors --}}
                    <input hidden type="number" id="input_red" name="red" value="">
                    <input hidden type="number" id="input_yellow" name="yellow" value="">
                    <input hidden type="number" id="input_blue" name="blue" value="">
                    <input hidden type="number" id="input_green" name="green" value=""><br>
                    <div hidden>Total Red: <span id="red">0</span>
                        Total Blue: <span id="blue">0</span>
                        Total Green: <span id="green">0</span>
                        Total Yellow: <span id="yellow">0</span>
                    </div>
                    {{--                    <<<<<<<<>>>>>>--}}
                </div>
                <center>
                    <button type="submit" name="submit" class="btn w3-indigo">Submit</button>
                </center>
            </form>


        </div>
    </div>
    <script type="text/javascript">
        $('#reload').click(function () {
            $.ajax({
                type: 'GET',
                url: 'reload-captcha',
                success: function (data) {
                    $(".captcha span").html(data.captcha);
                }
            });
        });


    </script>
@stop
