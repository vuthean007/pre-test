@extends('Layout.master')
@section('content')
    <div class="w3-container" style="margin-top:20px">
        <div class="sticky">
            <a href="#" onclick="javascript:history.go(-1)" id="back" class="btn w3-indigo w3-margin-bottom"><i class="fa fa-undo" aria-hidden="true"></i>
                Back</a>
            <a href="#" class="btn w3-indigo w3-margin-bottom" id="download"><i class="fa fa-print"></i>
                PDF</a>
        </div>
        <div class="w3-card-4 w3-round" id="invoice">
            <header class="w3-container text-center radius_header">
                <h1 class="h1">Pre-Employment Test </h1>
            </header>
            <form method="post" class="form-horizontal padding_form" action="">
                @csrf
                <div class="form-group">
                    <label class="control-label col-sm-2" for="name">Candidate Name:
                    </label>
                    <div class="col-sm-8">
                        <input disabled type="text" class="form-control" id="name" placeholder="Enter Name" name="name"
                               value="{{$user->name}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="sex">Sex:</label>
                    <div class="col-sm-8">
                        <label class="radio-inline"><input disabled type="radio" id="sex" name="sex"
                                                           value="male" {{ ($user->gender =="male")? "checked" : "" }}>Male</label>
                        <label class="radio-inline"><input disabled type="radio" id="sex" name="sex"
                                                           value="female" {{ ($user->gender =="female")? "checked" : "" }}>Female</label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="phone">Phone Number:</label>
                    <div class="col-sm-8">
                        <input disabled type="text" class="form-control" id="phone" placeholder="Enter Phone"
                               name="phone" value="{{$user->phone}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="position">Position Apply:</label>
                    <div class="col-sm-8">
                        <input disabled type="text" class="form-control" id="position" placeholder="Enter Position"
                               name="position" value="{{$user->apply}}">
                    </div>
                </div>
                <div class="container">
                    <h3><b>Part one- Your behavioral profile</b></h3>
                    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Each row of four words across, place an “X” in front of the one
                        word that most often applies to you.
                        Continue through all forty lines ensuring that each number is marked.
                        គូសសញ្ញា“X”ខាងមុខពាក្យមួយដែលសមស្របជាមួយអ្នកបំផុត ក្នុងចំនោមពាក្យទាំងបួនក្នុងជួរនីមួយៗ។
                        ត្រូវបានគូសគ្រប់ទាំងអស់ ៤០ជួរ។
                        If you are not sure which word” most applies” think of what your answer would have been when you
                        were a child?
                        ប្រសិនបើអ្នកមិនប្រាកដថាពាក្យណាមួយសមស្របជាមួយអ្នកបំផុតទេ
                        គិតពីអ្វីដែលអ្នកអាចជាចំលើយរបស់អ្នកកាលពីនៅក្មេង។
                    </p>
                </div>
                <center>
                    <table class="table table-condensed" style="width: 70%;">
                        <thead>
                        <tr>
                            <th>Question</th>
                            <th>Answer</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $index = 1?>
                        @foreach(unserialize($select_survey->all_surveys) as  $data_survey)
                            <tr class="active">
                                <td>{{$index++}}</td>
                                <td>{{$data_survey}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <table class="table table-condensed">
                        <tr>
                            <td>
                                <div class="w3-yellow w3-round-large" style="width: 60%;padding: 3%">
                                    <h3>
                                        Total Yellow: {{ ($select_survey->total_yellow)}}
                                    </h3>
                                </div>
                            </td>
                            <td>
                                <div class="w3-red w3-round-large" style="width: 60%;padding: 3%">
                                    <h3>
                                        Total Red: {{ ($select_survey->total_red)}}
                                    </h3>
                                </div>

                            </td>
                            <td>
                                <div class="w3-blue w3-round-large" style="width: 60%;padding: 3%">
                                    <h3>
                                        Total Blue: {{ ($select_survey->total_blue)}}
                                    </h3>
                                </div>
                            </td>
                            <td>
                                <div class="w3-green w3-round-large" style="width: 60%;padding: 3%">
                                    <h3>
                                        Total Green: {{ ($select_survey->total_green)}}
                                    </h3>
                                </div>
                            </td>
                        </tr>
                    </table>

                </center>
            </form>


        </div>
    </div>
    </div>





@stop

