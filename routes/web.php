<?php

use App\Question;
use App\Candidate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\QuestionController;
use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('change-password', 'ChangePasswordController@index')->middleware('verifylogin');
Route::post('change-password', 'ChangePasswordController@store')->name('change.password');
Route::get('/survey_failed',function (){
    return view('survey_failed');
});
Route::get('/insert_failed',function (){
    return view('insert_failed');
});

Route::get('/thanks',function (){
   return view('thanks');
})->middleware('user_empty');

Route::get('/', function () {
    $survey = \App\Question::all();
    return view('survey',compact('survey'));
})->name('survey')->middleware('user_empty');

Route::get('/question', function () {
    $count_question = Question::count();
    return view('question',compact('count_question'));
})->middleware('verifylogin');

Route::get('/after_login',function (){
   return view('after_login');
})->middleware('verifylogin');

Route::get('/dashboard', function () {
    $survey = Candidate::all();
    return view('dashboard',compact('survey'));
})->name('dashboard')->middleware('verifylogin');

Route::get('/question/view', function () {
    return view('Question.view_question');
})->middleware('verifylogin');

Route::prefix('question')->middleware('verifylogin')->group(function (){
    Route::post('/insert','QuestionController@insert');
    Route::get('/edit/{id}','QuestionController@edit');
    Route::post('/update/{id}','QuestionController@update');
    Route::delete('/delete/{id}','QuestionController@destroy');

});
Route::prefix('survey')->middleware('user_empty')->group(function (){
    Route::post('/insert','SurveyController@insert');
    Route::get('/review','SurveyController@review');
});
Route::get('survey/view/{id}','SurveyController@view')->middleware('verifylogin');
Route::get('survey/export_excel','SurveyController@export')->name('export')->middleware('verifylogin');
Route::post('search','SurveyController@search')->middleware('verifylogin');
Route::get('search_all','SurveyController@searchAll')->middleware('verifylogin');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin',function (){
    return view('auth.login');
});

Route::get('reload-captcha', 'SurveyController@reloadCaptcha');
